import ftfy
import sys
import subprocess
import mysql.connector
import argparse

# (1) Install required packages
# pip3 install -r requirements.txt
#
# (2) Use the command. Without parameters to monolith.tags, column tag_name in localhost with credentials root/root
# python3 encodingFixer.py 
# python3 encodingFixer.py -host HOSTNAME -d DATABASE -t TABLE -c COLUMN -u USERNAME -p PASSWORD

parser = argparse.ArgumentParser(description='Fix the encoding of the selected column')
parser.add_argument("-host", "--hostname", dest = "hostname", default = "localhost", help="Server name")
parser.add_argument("-d", "--database", dest = "database", default = "monolith", help="Database name")
parser.add_argument("-t", "--table", dest = "table", default = "tags", help="Table name")
parser.add_argument("-c", "--column", dest = "column", default = "tag_name", help="Column to fix")
parser.add_argument("-u", "--username", dest = "username", default = "root", help="User name")
parser.add_argument("-p", "--password", dest = "password", default = "root", help="Password")
parser.add_argument("-T", "--test", dest = "test", action='store_true', help="Dry run without updating the database")
parser.add_argument("-v", "-V", "--verbose", dest = "verbose", action='store_true', help="Display all the entries that will change")
args = parser.parse_args()

db = mysql.connector.connect(
  host=args.hostname,
  user=args.username,
  password=args.password,
  database=args.database,
)

query = "SELECT * FROM {}".format(args.table)
cursor = db.cursor()
cursor.execute(query)

field_names = [i[0] for i in cursor.description]
first_field = field_names[0]

rows = [{cursor.description[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
processed_count = 0;
updates = [];

for row in rows:
    original_text = row[args.column]
    decoded_text = ftfy.fix_text(original_text)
    
    # Only process the row if there is a missmatch between the original text and the decoded one
    if original_text != decoded_text:
        processed_count+=1;
        updates.append("UPDATE `{}`.`{}` SET `{}` = '{}' WHERE `{}` = {};"
            .format(args.database,args.table,args.column,decoded_text,first_field,row[first_field]))
        if args.verbose:
            print("ID: ", row[first_field])
            print("Ori: ", original_text)
            print("New: ", decoded_text)
            print('*****************************************')
            print('')

if not processed_count:
    print('No encoding problems in table {}.{}'.format(args.database,args.table))
    quit()
else:
    print('{} rows with encoding problems will be updated in {}.{}'.format(processed_count,args.database,args.table))

if not args.test:
    answer = input("Do you want to proceed? y/n: ") 
    if answer == "yes" or answer == "y" or answer == "Y": 
        for sql in updates:
            cursor.execute(sql)
        
        db.commit()
    elif answer == "no" or answer == "n" or answer == "N": 
        print('Nothing will be changed') 
    else: 
        print("Please enter yes or no") 

db.close()